//
//  SalesForceViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 10/8/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SBJson.h"
#import "AppDelegate.h"
#import "NSString+Base64.h"

@interface SalesForceViewController : UITableViewController{
    UIView *activityView;
}
@property (retain, nonatomic) IBOutlet UITextField *username;
@property (retain, nonatomic) IBOutlet UITextField *password;
@property (retain, nonatomic) IBOutlet UITextField *secretToken;
@property (retain, nonatomic) IBOutlet UIButton *integrateBtnClick;
- (IBAction)hideKeyboard:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *errorLabel;
- (IBAction)backButtonClick:(id)sender;
- (IBAction)integrateButtonClick:(id)sender;
- (void) submitData:(NSString *)urlString;
@end
