//
//  ForgotPasswordViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJson.h"
#import "AppDelegate.h"

@interface ForgotPasswordViewController : UITableViewController
@property (retain, nonatomic) IBOutlet UITextField *companyNameText;
@property (retain, nonatomic) IBOutlet UITextField *groupNameText;
@property (retain, nonatomic) IBOutlet UITextField *phoneNumberText;
@property (retain, nonatomic) IBOutlet UILabel *errorLabel;


- (IBAction)hideKeyboard:(id)sender;
- (IBAction)resetButtonClick:(id)sender;
- (IBAction)backButtonClick:(id)sender;

@end
