//
//  Table viewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "loginViewController.h"

@implementation loginViewController{
    Reachability* internetReachable;
}
@synthesize phoneNumberText;
@synthesize groupNameText;
@synthesize companyNameText;
@synthesize passwordText;
@synthesize checkBoxButton;
@synthesize checkedBoxButton;
@synthesize errorLabel;
@synthesize doneButton;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardDidShow:) 
													 name:UIKeyboardDidShowNotification 
												   object:nil];		
	} else {
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillShow:) 
													 name:UIKeyboardWillShowNotification 
												   object:nil];
	}
    flag=0;
    self.phoneNumberText.delegate=self;
    self.passwordText.delegate=self;
    //UIImageView* bgView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default@2x.png"]] autorelease];
    self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_image.png"]];
    //[self.mainView addSubview:bgView];
    //[self.mainView sendSubviewToBack:bgView];
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField { 
    if ([textField isEqual:self.passwordText]) {
        flag=0;        
        if(doneButton !=nil){
            [doneButton removeFromSuperview];
            doneButton=nil;
        }
    }else{
        flag=1;
    }        
}

- (void)keyboardWillShow:(NSNotification *)note {
    // if clause is just an additional precaution, you could also dismiss it
	if ([[[UIDevice currentDevice] systemVersion] floatValue] < 3.2) {
		[self addButtonToKeyboard];
	}
}

- (void)keyboardDidShow:(NSNotification *)note {
    if(flag==1){
	// if clause is just an additional precaution, you could also dismiss it
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
            [self addButtonToKeyboard];
        }
    }
}

- (void) addButtonToKeyboard {
	// create custom button
	doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
	doneButton.frame = CGRectMake(0, 163, 106, 53);
	doneButton.adjustsImageWhenHighlighted = NO;
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.0) {
		[doneButton setImage:[UIImage imageNamed:@"DoneUp.png"] forState:UIControlStateNormal];
		[doneButton setImage:[UIImage imageNamed:@"DoneDown.png"] forState:UIControlStateHighlighted];
	} else {        
		[doneButton setImage:[UIImage imageNamed:@"DoneUp.png"] forState:UIControlStateNormal];
		[doneButton setImage:[UIImage imageNamed:@"DoneDown.png"] forState:UIControlStateHighlighted];
	}
	[doneButton addTarget:self action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	// locate keyboard view
	UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
	UIView* keyboard;
	for(int i=0; i<[tempWindow.subviews count]; i++) {
		keyboard = [tempWindow.subviews objectAtIndex:i];
		// keyboard found, add the button
		if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
			if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
				[keyboard addSubview:doneButton];
		} else {            
			if([[keyboard description] hasPrefix:@"<UIKeyboard"] == YES){
				[keyboard addSubview:doneButton];               
            }
		}
	}
}

- (void)doneButtonAction:(id)sender{
    [doneButton removeFromSuperview];
    doneButton=nil;
    [self.phoneNumberText resignFirstResponder];
}


- (void)viewDidUnload
{   
    [self setPhoneNumberText:nil];
    [self setGroupNameText:nil];
    [self setCompanyNameText:nil];
    [self setPasswordText:nil];
    [self setCheckBoxButton:nil];
    [self setCheckedBoxButton:nil];
    [self setErrorLabel:nil];
    [self setMainView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)keyboardDidHide:(NSNotification *)note 
{       
    
}

- (IBAction)openLink:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://automapicapp.ap01.aws.af.cm/terms.html"]];
}

- (void) checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)    
    {
        case NotReachable:
        {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Error" message:@"Internet connection required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];  
            break;            
        }
        case ReachableViaWiFi:
        {
            break;            
        }
        case ReachableViaWWAN:
        {
            break;            
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    //internetReachable = [Reachability reachabilityForInternetConnection];
    //[internetReachable startNotifier];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    /*
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"user.plist"];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        NSMutableArray *users= [[NSMutableArray alloc] init];
        users= [NSMutableArray arrayWithContentsOfFile: plistPath];
        NSDictionary *user=[users objectAtIndex:0];
        NSString *remember= [user objectForKey:@"remember"];
        if([remember isEqualToString:@"1"]){
            
            NSString *phoneNumber= [user objectForKey:@"phonenumber"];
            NSString *password= [user objectForKey:@"password"];
            NSString *company= [user objectForKey:@"companyname"];
            NSString *group= [user objectForKey:@"groupname"];
            
            //NSString *url=[@"http://www.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
            //NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
            NSString *url=[@"https://automapic.phpfogapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
            url=[url stringByAppendingString:@"&password="];
            url=[url stringByAppendingString:password];
            url=[url stringByAppendingString:@"&company_name="];
            url=[url stringByAppendingString:company];
            url=[url stringByAppendingString:@"&group_name="];
            url=[url stringByAppendingString:group];
            url=[url stringByAppendingString:@"&param=mobile_phonecheck"];            
            
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            //   NSString *params = [[NSString alloc] initWithFormat:@"cname=bar&uname=value&password=pass&param=admincheck"];
            [request setHTTPMethod:@"GET"];
            // [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLResponse* response = nil;
            NSError *error = nil;
            NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSString* responseText;
            responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            // responseText=[response 
            if ([responseText isEqualToString:@"not match"])
            {                
                NSDictionary *userNew = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                [users removeAllObjects];
                [users addObject: userNew];
                [users writeToFile:plistPath atomically:YES];                
            }
            else{
                NSDictionary *results= [responseText JSONValue];
                NSMutableArray *result= [results objectForKey:@"result"];
                NSDictionary *user=[result objectAtIndex:0];
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.userName=[user objectForKey:@"username"];
                appDelegate.userId=[user objectForKey:@"id"];
                appDelegate.companyId=[user objectForKey:@"company_id"];
                appDelegate.groupId=[user objectForKey:@"group_id"];                
                [self performSegueWithIdentifier:@"RememberViewSeque" sender:self];                
            }            
        }    
        else{
            
        }
	} else {       
        NSMutableArray *users= [[NSMutableArray alloc] init];
        NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
        [users addObject: user];
        [users writeToFile:plistPath atomically:YES];
        
    }
     */
    phoneNumberText.text=@"";
    passwordText.text=@"";
    activityView.hidden=YES;
    activityView=nil;
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
        return YES;
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    else
        return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    return cell;
     
}
*/
- (IBAction)forgotButtonClick:(id)sender {
    [self performSegueWithIdentifier:@"ForgotPasswordSeque" sender:self];    
}

- (IBAction)validate:(id)sender {
    [sender resignFirstResponder];
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    NSString *phoneNumber=[phoneNumberText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password=[passwordText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //NSString *company=[companyNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
   // NSString *group=[groupNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    errorLabel.text=@"";
    errorLabel.hidden=TRUE;  
    
   // company=[company stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    password=[password stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    //group=[group stringByReplacingOccurrencesOfString:@" " withString:@"__"];
   /* 
    if([company length]<=0){
        errorLabel.text=@"Please enter company name";
        errorLabel.hidden=FALSE;        
    }    
    else if([group length]<=0){
        errorLabel.text=@"Please enter group name";
        errorLabel.hidden=FALSE;        
    }
    else
        */
    if([phoneNumber length]<=0){
        errorLabel.text=@"Please enter phone number";
        errorLabel.hidden=FALSE;        
    }
    else if([password length]<=0){
        errorLabel.text=@"Please enter password";
        errorLabel.hidden=FALSE;        
    }
    else{
        //  NSString *url=[@"http://www.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        //  NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        NSString *url=[@"https://automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        //NSString *url=[@"https://s2.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        url=[url stringByAppendingString:@"&password="];
        url=[url stringByAppendingString:password];
        //url=[url stringByAppendingString:@"&company_name="];
       // url=[url stringByAppendingString:company];
        //url=[url stringByAppendingString:@"&group_name="];
        //url=[url stringByAppendingString:group];
        //url=[url stringByAppendingString:@"&param=mobile_phonecheck"];
        url=[url stringByAppendingString:@"&param=ios_mobile_check"];
        int x=self.view.frame.size.width/2-50;
        int y=self.view.frame.size.height/2-44;
        activityView = [[UIView alloc] initWithFrame:CGRectMake(x, y, 100, 88)];
        activityView.backgroundColor=[UIColor blackColor];
        activityView.alpha=0.6;
        [activityView.layer setCornerRadius:5.0f];
        UIActivityIndicatorView *activityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.alpha = 1.0;
        activityIndicator.frame=CGRectMake(32, 13, 37, 37);
        [activityIndicator startAnimating];
        [activityView addSubview:activityIndicator];
        
        UILabel *message=[[UILabel alloc] initWithFrame:CGRectMake(0, 58, 100, 21)] ;
        message.text=@"Loading...";
        //userName.textColor
        message.opaque=NO;
        message.backgroundColor=[UIColor clearColor];
        message.textColor=[UIColor whiteColor];
        message.textAlignment=UITextAlignmentCenter;   
        message.font=[UIFont fontWithName:@"Verdana-Bold" size:15];
        message.adjustsFontSizeToFitWidth=NO;
        [activityView addSubview:message];    
        [self.view addSubview:activityView];
        [self performSelector:@selector(submitData:) withObject:url afterDelay:1];
    }
}

- (void) submitData:(NSString *)urlString{
    NSString *phoneNumber=[phoneNumberText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password=[passwordText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  //  NSString *company=[companyNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
   // NSString *group=[groupNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
   // company=[company stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    NSString *company=@"";
    NSString *group=@"";
    password=[password stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    //group=[group stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"GET"];
    NSURLResponse* response = nil;
    NSError *error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://automapicapp.com" withString:@"https://s2.automapicapp.com"];  
        //NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://s2.automapicapp.com" withString:@"https://automapic.phpfogapp.com"]; 
        NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
        [request1 setHTTPMethod:@"GET"];
        NSURLResponse* response1 = nil;
        NSError *error1 = nil;
        NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
        NSString* responseText;
        responseText = [[[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // responseText=[response
        
        if ([responseText isEqualToString:@"not match"])
        {
            errorLabel.text=@"Please check the credentials";
            errorLabel.hidden=FALSE; 
            activityView.hidden=YES;
            activityView=nil;
        }
        else if([responseText rangeOfString:@"result"].location != NSNotFound){    
            NSDictionary *results= [responseText JSONValue];
            NSMutableArray *result= [results objectForKey:@"result"];
            NSDictionary *user=[result objectAtIndex:0];
            NSString *status=[user objectForKey:@"status"];
            if ([status isEqualToString:@"unique"]) {
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.userName=[user objectForKey:@"username"];
                appDelegate.userId=[user objectForKey:@"id"];
                appDelegate.companyId=[user objectForKey:@"company_id"];
                appDelegate.groupId=[user objectForKey:@"group_id"];
                company=[user objectForKey:@"company_name"];
                company=[company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                company=[company stringByReplacingOccurrencesOfString:@" " withString:@"__"];
                group=[user objectForKey:@"group_name"];
                group=[group stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                group=[group stringByReplacingOccurrencesOfString:@" " withString:@"__"];
                
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];           
                
                NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
                // NSLog(plistPath);
                
                NSMutableArray *users= [[NSMutableArray alloc] init];
                users= [NSMutableArray arrayWithContentsOfFile: plistPath];
                if(checkedBoxButton.hidden){
                    NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber",@"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                    [users removeAllObjects];
                    [users addObject: user];
                }
                else{
                    NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: phoneNumber, @"phonenumber", password, @"password",company, @"companyname",group, @"groupname",@"1",@"remember", nil];
                    [users removeAllObjects];
                    [users addObject: user];
                    //   UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"checked" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //  [alert show];
                }
                [users writeToFile:plistPath atomically:YES]; 
                activityView.hidden=YES;
                activityView=nil;
                [self performSegueWithIdentifier:@"MainViewSeque" sender:self];             
            }else{
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.phoneNumber=phoneNumber;
                appDelegate.password=password;
                if (checkedBoxButton.hidden) {
                    appDelegate.remember=@"0";
                }else{
                    appDelegate.remember=@"1";
                }
                [self performSegueWithIdentifier:@"VerifyViewSeque" sender:self];
            }        
        }
        else{
            activityView.hidden=YES;
            activityView=nil;
            errorLabel.text=@"Please check the credentials";
            errorLabel.hidden=FALSE;         
        } 
    }
    else{
        NSString* responseText;
        responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // responseText=[response
        if ([responseText isEqualToString:@"not match"])
        {
            errorLabel.text=@"Please check the credentials";
            errorLabel.hidden=FALSE; 
            activityView.hidden=YES;
            activityView=nil;
        }
        else if([responseText rangeOfString:@"result"].location != NSNotFound){    
            NSDictionary *results= [responseText JSONValue];
            NSMutableArray *result= [results objectForKey:@"result"];
            NSDictionary *user=[result objectAtIndex:0];
            NSString *status=[user objectForKey:@"status"];
            if ([status isEqualToString:@"unique"]) {
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.userName=[user objectForKey:@"username"];
                appDelegate.userId=[user objectForKey:@"id"];
                appDelegate.companyId=[user objectForKey:@"company_id"];
                appDelegate.groupId=[user objectForKey:@"group_id"];
                company=[user objectForKey:@"company_name"];
                company=[company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                company=[company stringByReplacingOccurrencesOfString:@" " withString:@"__"];
                group=[user objectForKey:@"group_name"];
                group=[group stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                group=[group stringByReplacingOccurrencesOfString:@" " withString:@"__"];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];           
                
                NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
                // NSLog(plistPath);
                
                NSMutableArray *users= [[NSMutableArray alloc] init];
                users= [NSMutableArray arrayWithContentsOfFile: plistPath];
                if(checkedBoxButton.hidden){
                    NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber",@"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                    [users removeAllObjects];
                    [users addObject: user];
                }
                else{
                    NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: phoneNumber, @"phonenumber", password, @"password",company, @"companyname",group, @"groupname",@"1",@"remember", nil];
                    [users removeAllObjects];
                    [users addObject: user];
                    //   UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"checked" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //  [alert show];
                }
                [users writeToFile:plistPath atomically:YES]; 
                activityView.hidden=YES;
                activityView=nil;
                [self performSegueWithIdentifier:@"MainViewSeque" sender:self];             
            }else{
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.phoneNumber=phoneNumber;
                appDelegate.password=password;
                if (checkedBoxButton.hidden) {
                    appDelegate.remember=@"0";
                }else{
                    appDelegate.remember=@"1";
                }
                [self performSegueWithIdentifier:@"VerifyViewSeque" sender:self];
            }        
        }
        else{
            activityView.hidden=YES;
            activityView=nil;
            errorLabel.text=@"Please check the credentials";
            errorLabel.hidden=FALSE;         
        }   
    }
}

- (IBAction)checkBoxClick:(id)sender {
    checkBoxButton.hidden=YES;
    checkedBoxButton.hidden=NO;
}

- (IBAction)checkedBoxClick:(id)sender {
    checkedBoxButton.hidden=YES;
    checkBoxButton.hidden=NO;
}

- (IBAction)onExitHideKeyboard:(id)sender {
    [sender resignFirstResponder];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

- (void)dealloc {
    [phoneNumberText release];
    [groupNameText release];
    [companyNameText release];
    [passwordText release];
    [checkBoxButton release];
    [checkedBoxButton release];
    [errorLabel release];
    [_mainView release];
    [super dealloc];
}
@end
