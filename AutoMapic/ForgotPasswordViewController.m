//
//  ForgotPasswordViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@implementation ForgotPasswordViewController
@synthesize companyNameText;
@synthesize groupNameText;
@synthesize phoneNumberText;
@synthesize errorLabel;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_image.png"]];
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setCompanyNameText:nil];
    [self setGroupNameText:nil];
    [self setPhoneNumberText:nil];
    [self setErrorLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
        return YES;
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    else
        return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


- (IBAction)hideKeyboard:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)resetButtonClick:(id)sender {
    [sender resignFirstResponder];
    
    NSString *phoneNumber=[phoneNumberText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *company=[companyNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *group=[groupNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    errorLabel.text=@"";
    errorLabel.hidden=TRUE;
    errorLabel.textColor=[UIColor redColor];
    
    company=[company stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    group=[group stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    if([company length]<=0){        
        errorLabel.text=@"Please enter company name";
        errorLabel.hidden=FALSE;          
    }
    else if([group length]<=0){
        errorLabel.text=@"Please enter group name";
        errorLabel.hidden=FALSE;        
    }
    else if([phoneNumber length]<=0){
        errorLabel.text=@"Please enter phone number";
        errorLabel.hidden=FALSE;        
    }    
    else{
        
         
        //  NSString *url=[@"http://www.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        //  NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        NSString *urlString=[@"?param=user_forgot_password&telephone=" stringByAppendingString:phoneNumber];
        urlString=[urlString stringByAppendingString:@"&company_name="];
        urlString=[urlString stringByAppendingString:company];
        urlString=[urlString stringByAppendingString:@"&group_name="];
        urlString=[urlString stringByAppendingString:group];
        
        NSString *url=[@"https://automapicapp.com/mobile.php" stringByAppendingString:urlString];
       // NSString *url=[@"https://s2.automapicapp.com/mobile.php" stringByAppendingString:urlString];
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
        //   NSString *params = [[NSString alloc] initWithFormat:@"cname=bar&uname=value&password=pass&param=admincheck"];
        [request setHTTPMethod:@"GET"];
        // [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLResponse* response = nil;
        NSError *error = nil;
        NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];        
        if (error) {
            NSString *url1=[@"https://s2.automapicapp.com/mobile.php" stringByAppendingString:urlString];
            //NSString *url1=[@"https://automapic.phpfogapp.com/mobile.php" stringByAppendingString:urlString];
            NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]];
            //   NSString *params = [[NSString alloc] initWithFormat:@"cname=bar&uname=value&password=pass&param=admincheck"];
            [request1 setHTTPMethod:@"GET"];
            // [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLResponse* response1 = nil;
            NSError *error1 = nil;
            NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
            NSString* responseText;
            responseText = [[[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if([responseText isEqualToString:@"success"]){       
                errorLabel.textColor=[UIColor greenColor];
                errorLabel.text=@"Password reset link has been sent to your email";
                errorLabel.hidden=FALSE;
                phoneNumberText.text=@"";
                companyNameText.text=@"";
                groupNameText.text=@"";            
            }
            else{
                errorLabel.text=@"Please check the credentials";
                errorLabel.hidden=FALSE;             
            }
        }else{
            NSString* responseText;
            responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if([responseText isEqualToString:@"success"]){       
                errorLabel.textColor=[UIColor greenColor];
                errorLabel.text=@"Password reset link has been sent to your email";
                errorLabel.hidden=FALSE;
                phoneNumberText.text=@"";
                companyNameText.text=@"";
                groupNameText.text=@"";            
            }
            else{
                errorLabel.text=@"Please check the credentials";
                errorLabel.hidden=FALSE;             
            }
        }
    }    
}

- (IBAction)backButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)dealloc {
    [companyNameText release];
    [groupNameText release];
    [phoneNumberText release];
    [errorLabel release];
    [super dealloc];
}
@end
