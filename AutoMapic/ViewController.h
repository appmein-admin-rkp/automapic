//
//  ViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 11/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class Reachability;

@interface ViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>{
    IBOutlet UILabel *phoneNumberLabel;
    IBOutlet UITextField *phoneNumberText;
    IBOutlet UILabel *passwordLabel;
    IBOutlet UITextField *passwordText;
    IBOutlet UILabel *rememberMeLabel;
    IBOutlet UIButton *okButton;
    IBOutlet UILabel *errorLabel;
    IBOutlet UIButton *checkboxImage;
    IBOutlet UIButton *checkBoxClickedImage;
    IBOutlet UILabel *rememberUserLabel;
    IBOutlet UIButton *yesButton;
    IBOutlet UIButton *noButton;
    
}
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberText;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordText;
@property (strong, nonatomic) IBOutlet UIButton *checkboxImage;
@property (strong, nonatomic) IBOutlet UILabel *rememberMeLabel;
@property (strong, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxClickedImage;
@property (strong, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong, nonatomic) IBOutlet UILabel *rememberUserLabel;
@property (strong, nonatomic) IBOutlet UIButton *yesButton;
@property (strong, nonatomic) IBOutlet UIButton *noButton;
@property (retain, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (retain, nonatomic) IBOutlet UITextField *groupNameText;
@property (retain, nonatomic) IBOutlet UITextField *companyNameText;



- (IBAction)onExitHideKeyboard:(id)sender;
-(IBAction)validate:(id)sender;
-(IBAction)checkBoxClick:(id)sender;
-(IBAction)checkBoxClicked:(id)sender;
-(IBAction)yesButtonClicked:(id)sender;
-(IBAction)noButtonClicked:(id)sender;
- (void) checkNetworkStatus:(NSNotification *)notice;
- (void)keyboardDidShow:(NSNotification *)note;
- (void)keyboardDidHide:(NSNotification *)note;
@end
