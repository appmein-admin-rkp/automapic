//
//  CheckInViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 12/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CheckInViewController.h"
#import "SBJson.h"
#import "AppDelegate.h"


@implementation CheckInViewController
@synthesize checkOutUrl;
@synthesize usernameLabel;
@synthesize addressLabel;
@synthesize checkInButton;
@synthesize checkOutButton;
@synthesize locationManager;
@synthesize location;
@synthesize refreshIcon;
@synthesize flag;
@synthesize checkInActivityLabel;
@synthesize checkInActivityIndicator;
@synthesize feedbackView;
@synthesize companyName;
@synthesize feedbackNotes;
@synthesize daysSlider;
@synthesize daysLabel;
@synthesize checkoutHelp;
@synthesize afterFeedbackView;
@synthesize segmentControl;
@synthesize activityView;
@synthesize coordinatesLabel;
@synthesize captionLabel;
@synthesize refBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self   
                                             selector:@selector(becomeActive:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
	
    NSString *userName=@"Hi ";
    appDelegate.sFlag=@"0";
    appDelegate.rFlag=@"0";
    userName=[userName stringByAppendingString:appDelegate.userName];
    feedbackNotes.delegate=self;
    daysLabel.delegate=self;
    self.usernameLabel.text=userName;
    flag=@"load_checkin";
    self.locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    location = nil;
    self.daysLabel.delegate=self;
     [activityView.layer setCornerRadius:5.0f];
    /*
    NSDate *tomorrow = [[NSDate alloc] initWithTimeIntervalSinceNow:24 * 60 * 60];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [daysLabel setText:[dateFormatter stringFromDate:tomorrow]];
    */
    
    /*
    NSArray *options =[[NSArray alloc]initWithObjects:@"Above Average",@"Average",@"Below Average",@"Did not happen",nil];    
    group =[[MIRadioButtonGroup alloc]initWithFrame:CGRectMake(20, 120, 280, 66) andOptions:options andColumns:2];
    [options release];
    [self.feedbackView addSubview:group];
    [group setSelected:0];
    */
    /*
     NSArray *optionsFollow=[[NSArray alloc]initWithObjects:@"Days",@"Weeks",nil];
    followGroup =[[MIRadioButtonGroup alloc]initWithFrame:CGRectMake(83, 221, 160, 33) andOptions:optionsFollow andColumns:2];
    [optionsFollow release];
    [self.feedbackView addSubview:followGroup];
    [followGroup setSelected:0];
    */
    [super viewDidLoad];
}
- (void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.sFlag isEqualToString:@"1"]) {
        appDelegate.sFlag=@"0";
        UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceRemember"];
        [self presentViewController:myController animated:YES completion:nil];
        
    }else if ([appDelegate.rFlag isEqualToString:@"1"]){
        appDelegate.rFlag=@"0";
        UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceIntegrate"];
        [self presentViewController:myController animated:YES completion:nil];
    }
    [super viewDidAppear:animated];
}

- (void) setDate{
    dateSheet =[[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    [dateSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame= CGRectMake(0, 44, 0, 0);
    UIDatePicker *datePicker=[[UIDatePicker alloc] initWithFrame:pickerFrame];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setTimeZone:[NSTimeZone localTimeZone]];
    [datePicker setDate:[NSDate date]];
    [dateSheet addSubview:datePicker];
    [datePicker release];
    
    UIToolbar *controlToolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, dateSheet.bounds.size.width, 44)];
    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];
    
    UIBarButtonItem *spacer= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *setButton= [[UIBarButtonItem alloc] initWithTitle:@"Set" style:UIBarButtonItemStyleDone target:self action:@selector(dismissDate)];
    
    UIBarButtonItem *cancelButton=[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelDate)];
    
    [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer,setButton, nil]];
    [spacer release];
    [cancelButton release];
    [setButton release];
    [dateSheet addSubview:controlToolbar];
    [controlToolbar release];
    [dateSheet showInView:self.view];
    [dateSheet setBounds:CGRectMake(0,0, 320, 485)];    
}

- (void) cancelDate{
    [dateSheet dismissWithClickedButtonIndex:0 animated:YES];    
    [dateSheet release];
}

- (IBAction)settingsButtonClick:(id)sender {
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"user.plist"];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"salesforce.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        NSMutableArray *users= [[NSMutableArray alloc] init];
        users= [NSMutableArray arrayWithContentsOfFile: plistPath];
        NSDictionary *user=[users objectAtIndex:0];
        NSString *status= [user objectForKey:@"status"];
        if ([status isEqualToString:@"1"]) {
            UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceRemember"];
            [self presentViewController:myController animated:YES completion:nil];
        }else{
            UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceIntegrate"];
            [self presentViewController:myController animated:YES completion:nil];
        }
    }else{
        NSMutableArray *users= [[NSMutableArray alloc] init];
        NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"username",@"", @"password",@"", @"token",@"0",@"status", nil];
        [users addObject: user];
        [users writeToFile:plistPath atomically:YES];
        UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceIntegrate"];
        [self presentViewController:myController animated:YES completion:nil];
    }
}

- (IBAction)sendToSalesForce:(id)sender {
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"user.plist"];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"salesforce.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        NSMutableArray *users= [[NSMutableArray alloc] init];
        NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"username",@"", @"password",@"", @"token",@"0",@"status", nil];
        [users addObject: user];
        [users writeToFile:plistPath atomically:YES];
    }
    NSMutableArray *users= [[NSMutableArray alloc] init];
    users= [NSMutableArray arrayWithContentsOfFile: plistPath];
    NSDictionary *user=[users objectAtIndex:0];
    NSString *status= [user objectForKey:@"status"];
    if ([status isEqualToString:@"1"]) {
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *username= [user objectForKey:@"username"];
        NSString *password= [user objectForKey:@"password"];
        NSString *passStr=[password base64DecodedString];
        NSString *token= [user objectForKey:@"token"];
        
        NSString *url=[@"https://automapicapp.com/mobile.php?user_name=" stringByAppendingString:username];
        url=[url stringByAppendingString:@"&password="];
        url=[url stringByAppendingString:passStr];
        url=[url stringByAppendingString:@"&security_token="];
        url=[url stringByAppendingString:token];
        url=[url stringByAppendingString:@"&last_inserted_id="];
        url=[url stringByAppendingString:appDelegate.insertId];
        url=[url stringByAppendingString:@"&group_id="];
        url=[url stringByAppendingString:appDelegate.groupId];
        url=[url stringByAppendingString:@"&param=push_data_to_salesforce"];
        int x=self.view.frame.size.width/2-50;
        int y=self.view.frame.size.height/2-44;
        activityView1 = [[UIView alloc] initWithFrame:CGRectMake(x, y, 100, 88)];
        activityView1.backgroundColor=[UIColor blackColor];
        activityView1.alpha=0.6;
        [activityView1.layer setCornerRadius:5.0f];
        UIActivityIndicatorView *activityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.alpha = 1.0;
        activityIndicator.frame=CGRectMake(32, 13, 37, 37);
        [activityIndicator startAnimating];
        [activityView1 addSubview:activityIndicator];
        
        UILabel *message=[[UILabel alloc] initWithFrame:CGRectMake(0, 58, 100, 21)] ;
        message.text=@"Loading...";
        //userName.textColor
        message.opaque=NO;
        message.backgroundColor=[UIColor clearColor];
        message.textColor=[UIColor whiteColor];
        message.textAlignment=UITextAlignmentCenter;
        message.font=[UIFont fontWithName:@"Verdana-Bold" size:15];
        message.adjustsFontSizeToFitWidth=NO;
        [activityView1 addSubview:message];
        [self.afterFeedbackView addSubview:activityView];
        [self performSelector:@selector(submitSalesForceData:) withObject:url afterDelay:1];
    }else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please go to settings and enter   SalesForce credentials" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void) submitSalesForceData:(NSString *)urlString{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"GET"];
    NSURLResponse* response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://automapicapp.com" withString:@"https://s2.automapicapp.com"];
        //NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://s2.automapicapp.com" withString:@"https://automapic.phpfogapp.com"];
        NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
        [request1 setHTTPMethod:@"GET"];
        NSURLResponse* response1 = nil;
        NSError *error1 = nil;
        NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
        NSString *responseText1;
        responseText1 = [[[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([responseText1 isEqualToString:@"no"])
        {
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Wrong SalesForce credentials" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if([responseText1 isEqualToString:@"success"]){
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"Shared with SalesForce successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if([responseText1 isEqualToString:@"fail"]){
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to share with SalesForce" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else{
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to share with SalesForce" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else{
        NSString* responseText;
        responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([responseText isEqualToString:@"no"])
        {
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Wrong SalesForce credentials" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if([responseText isEqualToString:@"success"]){
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"Shared with SalesForce successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if([responseText isEqualToString:@"fail"]){
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to share with SalesForce" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }else{
            activityView1.hidden=YES;
            activityView1=nil;
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to share with SalesForce" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (IBAction)valueChanged:(id)sender {
    NSUInteger index = (NSUInteger)(daysSlider.value + 0.5); // Round the number.
    [daysSlider setValue:index animated:NO];
}

- (IBAction)sendToYammer:(id)sender {
    UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"YammerPost"];
    [self presentViewController:myController animated:YES completion:nil];
}

- (void) dismissDate{
    NSArray *listOfViews=[dateSheet subviews];
    NSDate *dte;
    for (UIView *subview in listOfViews){
        if ([subview isKindOfClass:[UIDatePicker class]]) {
            dte=[(UIDatePicker *)subview date];  
        }
    }
    //if ([dte compare:tdy]==NSOrderedDescending) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        [daysLabel setText:[dateFormatter stringFromDate:dte]];    
        [dateFormatter release];
        //[dte release];
        [dateSheet dismissWithClickedButtonIndex:0 animated:YES];
        [dateSheet release];    
    /*    
    }else{
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select correct date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
     */
}

- (void)becomeActive:(NSNotification *)notification {   
    
    if (checkOutButton.hidden && checkInButton.hidden) {
                
    }else{
        addressLabel.text=@"";
        [checkInActivityIndicator startAnimating];
        checkInActivityIndicator.hidden=NO;
        checkInActivityLabel.hidden=NO;
        if(checkOutButton.hidden)
            self.flag=@"load_checkin";
        else 
            self.flag=@"load_checkout";
        [locationManager startUpdatingLocation];
    }
}

- (void)viewDidUnload
{
    [self setUsernameLabel:nil];
    [self setAddressLabel:nil];
    [self setCheckInButton:nil];
    [self setCheckOutButton:nil];
    [self setRefreshIcon:nil];
    [self setCheckInActivityIndicator:nil];
    [self setCheckInActivityLabel:nil];
    [self setFeedbackView:nil];
    [self setCompanyName:nil];
    [self setFeedbackNotes:nil];
    [self setDaysSlider:nil];
    [self setDaysLabel:nil];
    [self setCheckoutHelp:nil];
    [self setAfterFeedbackView:nil];
    [self setSegmentControl:nil];
    [self setActivityView:nil];
    [self setCoordinatesLabel:nil];
    [self setCaptionLabel:nil];
    [self setRefBtn:nil];
    [self setRefreshLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
        return YES;
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    else
        return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //if(textField isEqual:daysLabel){
	[textField resignFirstResponder];
	return YES;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    [self setDate];
    return NO;
}

- (IBAction)refBtnClick:(id)sender {
}

- (IBAction)newMeeting:(id)sender {
    self.afterFeedbackView.hidden=YES;
    self.checkInButton.hidden=NO;
    self.checkOutButton.hidden=YES;
    self.usernameLabel.hidden=NO;
    self.addressLabel.hidden=NO;
    self.refreshIcon.hidden=NO;
    self.captionLabel.hidden=NO;
    self.refreshLabel.hidden=NO;
    self.refBtn.hidden=NO;
    self.coordinatesLabel.hidden=NO;
    self.checkInActivityIndicator.hidden=YES;
    self.checkInActivityLabel.hidden=YES;
    self.checkoutHelp.hidden=YES;    
    self.companyName.text=@"";
    //[segmentControl setSelectedSegmentIndex:0];
    self.daysSlider.value=0.0;
    self.daysLabel.text=@"";
    self.feedbackNotes.text=@"";
}

- (IBAction)refresh:(id)sender {
    self.flag=@"refresh"; 
    [locationManager startUpdatingLocation];
}

- (IBAction)checkOutClicked:(id)sender {
    self.flag=@"checkout"; 
    [locationManager startUpdatingLocation];
}

- (IBAction)checkInClicked:(id)sender {
    self.flag=@"checkin";   
    [locationManager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    if([self.flag isEqualToString:@"load"]){
        self.location=newLocation;
        int degrees = newLocation.coordinate.latitude;
        double decimal = fabs(newLocation.coordinate.latitude - degrees);
        int minutes = decimal * 60;
        double seconds = decimal * 3600 - minutes * 60;
        int sec=(seconds +0.50);
        NSString *deg=[NSString stringWithUTF8String:"\xC2\xB0"];
        NSString *lat = [NSString stringWithFormat:@"%d%@%d'%i\"",
                         degrees,deg, minutes, sec];
        
        degrees = newLocation.coordinate.longitude;
        decimal = fabs(newLocation.coordinate.longitude - degrees);
        minutes = decimal * 60;
        seconds = decimal * 3600 - minutes * 60;
        sec=(seconds +0.50);
        NSString *longt = [NSString stringWithFormat:@"%d%@%d'%i\"",
                           degrees,deg, minutes, sec];
        captionLabel.text=@"You are at ";
        coordinatesLabel.text=[NSString stringWithFormat:@"%@, %@",lat,longt];
        CLGeocoder *myGeocoder = [[CLGeocoder alloc] init];
        [myGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil &&
                [placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSArray *add=[placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
                /* We received the results */
                NSString *address=@"Your approximate address is ";
                for (int i = 0; i< add.count; i++){
                    address= [address stringByAppendingString:[add objectAtIndex:i]];
                    address= [address stringByAppendingString:@", "];
                }
                address=[address substringToIndex:[address length]-2];   
                addressLabel.text=address;                
                CGSize fontSize = [addressLabel.text sizeWithFont:addressLabel.font];
                double finalHeight = fontSize.height * addressLabel.numberOfLines;
                double finalWidth = addressLabel.frame.size.width; //expected width of label
                CGSize theStringSize = [addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:addressLabel.lineBreakMode];
                int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
                for(int i=0; i<newLinesToPad; i++)
                    addressLabel.text = [addressLabel.text stringByAppendingString:@"\n "];
            }
            else if (error == nil &&
                     [placemarks count] == 0){
                 addressLabel.text=@"";
            }
            else if (error != nil){
                addressLabel.text=@"";
            } 
        }];
        self.flag=@"";
        [locationManager stopUpdatingLocation];
    }
    else if([self.flag isEqualToString:@"load_checkin"]){
        self.location=newLocation;
        int degrees = newLocation.coordinate.latitude;
        double decimal = fabs(newLocation.coordinate.latitude - degrees);
        int minutes = decimal * 60;
        double seconds = decimal * 3600 - minutes * 60;
        int sec=(seconds +0.50);
        NSString *deg=[NSString stringWithUTF8String:"\xC2\xB0"];
        NSString *lat = [NSString stringWithFormat:@"%d%@%d'%i\"",
                         degrees,deg, minutes, sec];
        
        degrees = newLocation.coordinate.longitude;
        decimal = fabs(newLocation.coordinate.longitude - degrees);
        minutes = decimal * 60;
        seconds = decimal * 3600 - minutes * 60;
        sec=(seconds +0.50);
        NSString *longt = [NSString stringWithFormat:@"%d%@%d'%i\"",
                           degrees,deg, minutes, sec];
        captionLabel.text=@"You are at ";
        coordinatesLabel.text=[NSString stringWithFormat:@"%@, %@",lat,longt];
        CLGeocoder *myGeocoder = [[CLGeocoder alloc] init];
        [myGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil &&
                [placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSArray *add=[placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
                /* We received the results */
                NSString *address=@"Your approximate address is ";
                for (int i = 0; i< add.count; i++){
                    address= [address stringByAppendingString:[add objectAtIndex:i]];
                    address= [address stringByAppendingString:@", "];
                }
                address=[address substringToIndex:[address length]-2];   
                addressLabel.text=address;
                CGSize fontSize = [addressLabel.text sizeWithFont:addressLabel.font];
                double finalHeight = fontSize.height * addressLabel.numberOfLines;
                double finalWidth = addressLabel.frame.size.width; //expected width of label
                CGSize theStringSize = [addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:addressLabel.lineBreakMode];
                int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
                for(int i=0; i<newLinesToPad; i++)
                    addressLabel.text = [addressLabel.text stringByAppendingString:@"\n "];
            }
            else if (error == nil &&
                     [placemarks count] == 0){
                addressLabel.text=@"";
            }
            else if (error != nil){
                addressLabel.text=@"";
            } 
            self.flag=@"";
            checkInButton.hidden=NO;
            [checkInActivityIndicator stopAnimating];
            checkInActivityIndicator.hidden=YES;
            checkInActivityLabel.hidden=YES;
            checkoutHelp.hidden=YES;
        }];
        [locationManager stopUpdatingLocation];
    }    
    else if([self.flag isEqualToString:@"load_checkout"]){
        self.location=newLocation;
        int degrees = newLocation.coordinate.latitude;
        double decimal = fabs(newLocation.coordinate.latitude - degrees);
        int minutes = decimal * 60;
        double seconds = decimal * 3600 - minutes * 60;
        int sec=(seconds +0.50);
        NSString *deg=[NSString stringWithUTF8String:"\xC2\xB0"];
        NSString *lat = [NSString stringWithFormat:@"%d%@%d'%i\"",
                         degrees,deg, minutes, sec];
        
        degrees = newLocation.coordinate.longitude;
        decimal = fabs(newLocation.coordinate.longitude - degrees);
        minutes = decimal * 60;
        seconds = decimal * 3600 - minutes * 60;
        sec=(seconds +0.50);
        NSString *longt = [NSString stringWithFormat:@"%d%@%d'%i\"",
                           degrees,deg, minutes, sec];
        captionLabel.text=@"You checked in at ";
        coordinatesLabel.text=[NSString stringWithFormat:@"%@, %@",lat,longt];
        CLGeocoder *myGeocoder = [[CLGeocoder alloc] init];
        [myGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil &&
                [placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSArray *add=[placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
                /* We received the results */
                //NSString *address=@"You checked in at ";
                NSString *address=@"Your approximate address is ";
                for (int i = 0; i< add.count; i++){
                    address= [address stringByAppendingString:[add objectAtIndex:i]];
                    address= [address stringByAppendingString:@", "];
                }
                address=[address substringToIndex:[address length]-2];   
                addressLabel.text=address;
                CGSize fontSize = [addressLabel.text sizeWithFont:addressLabel.font];
                double finalHeight = fontSize.height * addressLabel.numberOfLines;
                double finalWidth = addressLabel.frame.size.width; //expected width of label
                CGSize theStringSize = [addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:addressLabel.lineBreakMode];
                int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
                for(int i=0; i<newLinesToPad; i++)
                    addressLabel.text = [addressLabel.text stringByAppendingString:@"\n "];
            }
            else if (error == nil &&
                     [placemarks count] == 0){
                //addressLabel.text=@"";
            }
            else if (error != nil){
                //addressLabel.text=@"";
            } 
            self.flag=@"";
            checkOutButton.hidden=NO;
            [checkInActivityIndicator stopAnimating];
            checkInActivityIndicator.hidden=YES;
            checkInActivityLabel.hidden=YES;
            checkoutHelp.hidden=NO;
        }];        
        [locationManager stopUpdatingLocation];
    } 
    else if([self.flag isEqualToString:@"refresh"]){
        self.location=newLocation;
        int degrees = newLocation.coordinate.latitude;
        double decimal = fabs(newLocation.coordinate.latitude - degrees);
        int minutes = decimal * 60;
        double seconds = decimal * 3600 - minutes * 60;
        int sec=(seconds +0.50);
        NSString *deg=[NSString stringWithUTF8String:"\xC2\xB0"];
        NSString *lat = [NSString stringWithFormat:@"%d%@%d'%i\"",
                         degrees,deg, minutes, sec];
        
        degrees = newLocation.coordinate.longitude;
        decimal = fabs(newLocation.coordinate.longitude - degrees);
        minutes = decimal * 60;
        seconds = decimal * 3600 - minutes * 60;
        sec=(seconds +0.50);
        NSString *longt = [NSString stringWithFormat:@"%d%@%d'%i\"",
                           degrees,deg, minutes, sec];
        captionLabel.text=@"You are at ";
        coordinatesLabel.text=[NSString stringWithFormat:@"%@, %@",lat,longt];
        CLGeocoder *myGeocoder = [[CLGeocoder alloc] init];
        [myGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil &&
                [placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSArray *add=[placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
                /* We received the results */
                //if(checkInButton.hidden){
                //NSString *address=@"You are at ";
                NSString *address=@"Your approximate address is ";
                for (int i = 0; i< add.count; i++){
                    address= [address stringByAppendingString:[add objectAtIndex:i]];
                    address= [address stringByAppendingString:@", "];
                }
                address=[address substringToIndex:[address length]-2];   
                addressLabel.text=address;
                CGSize fontSize = [addressLabel.text sizeWithFont:addressLabel.font];
                double finalHeight = fontSize.height * addressLabel.numberOfLines;
                double finalWidth = addressLabel.frame.size.width; //expected width of label
                CGSize theStringSize = [addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:addressLabel.lineBreakMode];
                int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
                for(int i=0; i<newLinesToPad; i++)
                    addressLabel.text = [addressLabel.text stringByAppendingString:@"\n "];
            }
            else if (error == nil &&
                     [placemarks count] == 0){
                addressLabel.text=@"";
            }
            else if (error != nil){
                addressLabel.text=@"";
            } 
        }];
        self.flag=@"";        
        [locationManager stopUpdatingLocation];        
    }
    else if([self.flag isEqualToString:@"checkin"]){
        self.location=newLocation;
        self.flag=@"";
        int degrees = newLocation.coordinate.latitude;
        double decimal = fabs(newLocation.coordinate.latitude - degrees);
        int minutes = decimal * 60;
        double seconds = decimal * 3600 - minutes * 60;
        int sec=(seconds +0.50);
        NSString *deg=[NSString stringWithUTF8String:"\xC2\xB0"];
        NSString *lat = [NSString stringWithFormat:@"%d%@%d'%i\"",
                         degrees,deg, minutes, sec];
        
        degrees = newLocation.coordinate.longitude;
        decimal = fabs(newLocation.coordinate.longitude - degrees);
        minutes = decimal * 60;
        seconds = decimal * 3600 - minutes * 60;
        sec=(seconds +0.50);
        NSString *longt = [NSString stringWithFormat:@"%d%@%d'%i\"",
                           degrees,deg, minutes, sec];
        captionLabel.text=@"You checked in at ";
        coordinatesLabel.text=[NSString stringWithFormat:@"%@, %@",lat,longt];
        
        NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%g",newLocation.coordinate.latitude];
        NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%g",newLocation.coordinate.longitude];
        
        double mktime=(double)[[NSDate date] timeIntervalSince1970];
        NSString *mkTme=[NSString stringWithFormat:@"%f", mktime];
       
        
        NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
        [dayFormatter setDateFormat:@"dd"];
        NSString *day = [dayFormatter stringFromDate:[NSDate date]];
        
        NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
        [monthFormatter setDateFormat:@"MM"];
        NSString *month = [monthFormatter stringFromDate:[NSDate date]];
        
        NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
        [yearFormatter setDateFormat:@"YYYY"];
        NSString *year = [yearFormatter stringFromDate:[NSDate date]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"E MMM dd YYYY HH:mm:ss"];
        NSString *dte = [dateFormatter stringFromDate:[NSDate date]];
        dte=[dte stringByReplacingOccurrencesOfString:@" " withString:@"_"];       
       
        CLGeocoder *myGeocoder = [[CLGeocoder alloc] init];
        [myGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil &&
                [placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSArray *add=[placemark.addressDictionary objectForKey:@"FormattedAddressLines"];                
                //NSString *address=@"You checked in at ";
                NSString *address=@"Your approximate address is ";
                for (int i = 0; i< add.count; i++){
                    address= [address stringByAppendingString:[add objectAtIndex:i]];
                    address= [address stringByAppendingString:@", "];
                }
                address=[address substringToIndex:[address length]-2];   
                addressLabel.text=address;
                CGSize fontSize = [addressLabel.text sizeWithFont:addressLabel.font];
                double finalHeight = fontSize.height * addressLabel.numberOfLines;
                double finalWidth = addressLabel.frame.size.width; //expected width of label
                CGSize theStringSize = [addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:addressLabel.lineBreakMode];
                int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
                for(int i=0; i<newLinesToPad; i++)
                    addressLabel.text = [addressLabel.text stringByAppendingString:@"\n "];
            }
            else if (error == nil &&
                     [placemarks count] == 0){
                addressLabel.text=@"";
            }
            else if (error != nil){
                addressLabel.text=@"";
            } 
        }];        
        NSString *addr=[addressLabel.text stringByReplacingOccurrencesOfString:@", " withString:@"_"];
        addr=[addr stringByReplacingOccurrencesOfString:@"You are at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"You are at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"You are at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"You checked in at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"Your approximate address is " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@" " withString:@"*"];
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *userId=appDelegate.userId;
       // NSString *url=[@"http://www.automapicapp.com/mobile.php?param=mobile_check_in_details&userid=" stringByAppendingString:userId];
        //NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?param=mobile_check_in_details&userid=" stringByAppendingString:userId];
        
        NSString *urlString=[@"?param=mobile_check_in_details&userid=" stringByAppendingString:userId];
        urlString=[urlString stringByAppendingString:@"&group_id="];
        urlString=[urlString stringByAppendingString:appDelegate.groupId];
        urlString=[urlString stringByAppendingString:@"&lat="];
        urlString=[urlString stringByAppendingString:currentLatitude];
        urlString=[urlString stringByAppendingString:@"&lng="];
        urlString=[urlString stringByAppendingString:currentLongitude];
        urlString=[urlString stringByAppendingString:@"&checkTime="];
        urlString=[urlString stringByAppendingString:dte];
        urlString=[urlString stringByAppendingString:@"&checkMktime="];
        urlString=[urlString stringByAppendingString:mkTme];
        urlString=[urlString stringByAppendingString:@"&checkDate="];
        urlString=[urlString stringByAppendingString:day];
        urlString=[urlString stringByAppendingString:@"&checkMonth="];
        urlString=[urlString stringByAppendingString:month];
        urlString=[urlString stringByAppendingString:@"&checkYear="];
        urlString=[urlString stringByAppendingString:year];
        urlString=[urlString stringByAppendingString:@"&address="];
        urlString=[urlString stringByAppendingString:addr];
        
        NSString *str=[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *url=[@"https://automapicapp.com/mobile.php" stringByAppendingString:str];
        //NSString *url=[@"https://s2.automapicapp.com/mobile.php" stringByAppendingString:str];
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"GET"];
        NSURLResponse* response = nil;
        NSError *error = nil;
        NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];        
        if (error) {
            NSString *url1=[@"https://s2.automapicapp.com/mobile.php" stringByAppendingString:str];
            //NSString *url1=[@"https://automapic.phpfogapp.com/mobile.php" stringByAppendingString:str];
            NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]];
            [request1 setHTTPMethod:@"GET"];
            NSURLResponse* response1 = nil;
            NSError *error1 = nil;
            NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1]; 
            NSString* responseText;
            responseText = [[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding];
            responseText=[responseText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            // UIAlertView *alert1= [[UIAlertView alloc] initWithTitle:@"Info" message:responseText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            // [alert1 show];
            //NSLog(responseText);
            if([responseText isEqualToString:@"not inserted"]){
                
            }else{
                // NSDictionary *results= [responseText JSONValue];
                //  NSMutableArray *result= [results objectForKey:@"result"];
                //  NSDictionary *user=[result objectAtIndex:0];
                //AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                NSString *inId=responseText;
                appDelegate.insertId=inId;
                
                //checkInButton.hidden=YES;
                //checkOutButton.hidden=NO;
                //checkoutHelp.hidden=NO;
                checkOutButton.alpha=0;
                checkOutButton.hidden=NO;
                checkoutHelp.alpha=0;
                checkoutHelp.hidden=NO;
                [UIView beginAnimations:@"fadeIn" context:nil];
                [UIView setAnimationDuration:1.0];
                ///theView.alpha = 1.0f;
                checkInButton.alpha=0;
                checkOutButton.alpha=1;
                checkoutHelp.alpha=1;
                //checkOutButton.hidden=NO;
                //checkoutHelp.hidden=NO;
                [UIView commitAnimations];
                checkInButton.hidden=YES;
                checkInButton.alpha=1;
            }
        }else{
            NSString* responseText;
            responseText = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            responseText=[responseText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            // UIAlertView *alert1= [[UIAlertView alloc] initWithTitle:@"Info" message:responseText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            // [alert1 show];
            if([responseText isEqualToString:@"not inserted"]){
                
            }else{
                // NSDictionary *results= [responseText JSONValue];
                //  NSMutableArray *result= [results objectForKey:@"result"];
                //  NSDictionary *user=[result objectAtIndex:0];
                //AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                NSString *inId=responseText;
                appDelegate.insertId=inId;
                checkOutButton.alpha=0;
                checkOutButton.hidden=NO;
                checkoutHelp.alpha=0;
                checkoutHelp.hidden=NO;
                [UIView beginAnimations:@"fadeIn" context:nil];
                [UIView setAnimationDuration:1.0];
                ///theView.alpha = 1.0f;
                checkInButton.alpha=0;
                checkOutButton.alpha=1;
                checkoutHelp.alpha=1;
                //checkOutButton.hidden=NO;
                //checkoutHelp.hidden=NO;
                [UIView commitAnimations];
                checkInButton.hidden=YES;
                checkInButton.alpha=1;
            }        
        }        
        [locationManager stopUpdatingLocation];
    }
    else if([self.flag isEqualToString:@"checkout"]){
        self.location=newLocation;
        self.flag=@"";
        int degrees = newLocation.coordinate.latitude;
        double decimal = fabs(newLocation.coordinate.latitude - degrees);
        int minutes = decimal * 60;
        double seconds = decimal * 3600 - minutes * 60;
        int sec=(seconds +0.50);
        NSString *deg=[NSString stringWithUTF8String:"\xC2\xB0"];
        NSString *lat = [NSString stringWithFormat:@"%d%@%d'%i\"",
                         degrees,deg, minutes, sec];
        
        degrees = newLocation.coordinate.longitude;
        decimal = fabs(newLocation.coordinate.longitude - degrees);
        minutes = decimal * 60;
        seconds = decimal * 3600 - minutes * 60;
        sec=(seconds +0.50);
        NSString *longt = [NSString stringWithFormat:@"%d%@%d'%i\"",
                           degrees,deg, minutes, sec];
        captionLabel.text=@"You are at ";
        coordinatesLabel.text=[NSString stringWithFormat:@"%@, %@",lat,longt];
        NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%g",newLocation.coordinate.latitude];
        NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%g",newLocation.coordinate.longitude];
       
        double mktime=(double)[[NSDate date] timeIntervalSince1970];
        NSString *mkTme=[NSString stringWithFormat:@"%f", mktime];
               
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"E MMM dd YYYY HH:mm:ss"];
        NSString *dte = [dateFormatter stringFromDate:[NSDate date]];
        dte=[dte stringByReplacingOccurrencesOfString:@" " withString:@"_"];     
        

        CLGeocoder *myGeocoder = [[CLGeocoder alloc] init];
        [myGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil &&
                [placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSArray *add=[placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
                //NSString *address=@"You are at ";
                NSString *address=@"Your approximate address is ";
                for (int i = 0; i< add.count; i++){
                    address= [address stringByAppendingString:[add objectAtIndex:i]];
                    address= [address stringByAppendingString:@", "];
                }
                address=[address substringToIndex:[address length]-2];   
                addressLabel.text=address;
                CGSize fontSize = [addressLabel.text sizeWithFont:addressLabel.font];
                double finalHeight = fontSize.height * addressLabel.numberOfLines;
                double finalWidth = addressLabel.frame.size.width; //expected width of label
                CGSize theStringSize = [addressLabel.text sizeWithFont:addressLabel.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:addressLabel.lineBreakMode];
                int newLinesToPad = (finalHeight - theStringSize.height) / fontSize.height;
                for(int i=0; i<newLinesToPad; i++)
                    addressLabel.text = [addressLabel.text stringByAppendingString:@"\n "];
            }
            else if (error == nil &&
                     [placemarks count] == 0){
                addressLabel.text=@"";
            }
            else if (error != nil){
                addressLabel.text=@"";
            } 
        }]; 
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *iId=appDelegate.insertId;
        
        NSString *addr=[addressLabel.text stringByReplacingOccurrencesOfString:@", " withString:@"_"];
        addr=[addr stringByReplacingOccurrencesOfString:@"You are at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"You are at " withString:@""];        
        addr=[addr stringByReplacingOccurrencesOfString:@"You are at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"You checked in at " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@"Your approximate address is " withString:@""];
        addr=[addr stringByReplacingOccurrencesOfString:@" " withString:@"*"];
       // NSString *url=[@"http://www.automapicapp.com/mobile.php?param=userupdate&id=" stringByAppendingString:iId];
        //NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?param=userupdate&id=" stringByAppendingString:iId];
        NSString *url=[@"https://automapicapp.com/mobile.php?param=ios_userupdate&id=" stringByAppendingString:iId];
        //NSString *url=[@"https://s2.automapicapp.com/mobile.php?param=ios_userupdate&id=" stringByAppendingString:iId];
        
        NSString *urlStr=@"";
        urlStr=[urlStr stringByAppendingString:@"&group_id="];
        urlStr=[urlStr stringByAppendingString:appDelegate.groupId];
        urlStr=[urlStr stringByAppendingString:@"&lat="];
        urlStr=[urlStr stringByAppendingString:currentLatitude];
        urlStr=[urlStr stringByAppendingString:@"&lng="];
        urlStr=[urlStr stringByAppendingString:currentLongitude];
        urlStr=[urlStr stringByAppendingString:@"&checkTime="];
        urlStr=[urlStr stringByAppendingString:dte];
        urlStr=[urlStr stringByAppendingString:@"&checkMktime="];
        urlStr=[urlStr stringByAppendingString:mkTme];
        urlStr=[urlStr stringByAppendingString:@"&address="];
        urlStr=[urlStr stringByAppendingString:addr];
        
        NSString *str=[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        url=[url stringByAppendingString:str];
        self.checkOutUrl=url;
        self.feedbackView.alpha=0;
        self.feedbackView.hidden=NO;
        [UIView beginAnimations:@"fadeIn" context:nil];
        [UIView setAnimationDuration:1.0];
        self.checkInButton.hidden=YES;
        self.checkOutButton.hidden=YES;
        self.usernameLabel.hidden=YES;
        self.addressLabel.hidden=YES;
        self.refreshIcon.hidden=YES;
        self.coordinatesLabel.hidden=YES;
        self.captionLabel.hidden=YES;
        self.refBtn.hidden=YES;
        self.checkInActivityIndicator.hidden=YES;
        self.checkInActivityLabel.hidden=YES;
        self.refreshLabel.hidden=YES;
        self.checkoutHelp.hidden=YES;
        self.feedbackView.alpha=1;
        [UIView commitAnimations];
        
     /*
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"GET"];
        NSURLResponse* response = nil;
        NSError *error = nil;
        NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString* responseText;
        responseText = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        responseText=[responseText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([responseText isEqualToString:@"true"]){
            checkInButton.hidden=YES;
            checkOutButton.hidden=YES;
            usernameLabel.hidden=YES;
            addressLabel.hidden=YES;
            refreshIcon.hidden=YES;
            checkInActivityIndicator.hidden=YES;
            checkInActivityLabel.hidden=YES;
            checkoutHelp.hidden=YES;
            feedbackView.hidden=NO;
        }       
          */       
      [locationManager stopUpdatingLocation];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    /* keyboard is visible, move views */
    self.view.frame=CGRectMake(0, -180, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    /* resign first responder, hide keyboard, move views */
    self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
}

- (void) textViewDidBeginEditing:(UITextView *)textView{
    self.view.frame=CGRectMake(0, -190, self.view.frame.size.width, self.view.frame.size.height);
}

-(void) textViewDidEndEditing:(UITextView *)textView{
    self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)keyboardDidShow:(NSNotification *)note 
{
    /* move your views here */
    // self.view.center=CGPointMake(0, 0);
}
- (void)keyboardDidHide:(NSNotification *)note 
{
    /* move your views here */
    self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
}

- (IBAction)feedBackSubmit:(id)sender {
    [daysLabel resignFirstResponder];
    [feedbackNotes resignFirstResponder];
    NSString *companyNme=[companyName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    companyNme=[companyNme stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    NSString *noDays=[daysLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *feedbackComments=[feedbackNotes.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    feedbackComments=[feedbackComments stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    //int meetingRating=group.getSelected;
    //int meetingRating=segmentControl.selectedSegmentIndex;
    int meetingRating=(int)(daysSlider.value);
    NSString *feedBackR=@"";
    if(meetingRating==0)
        feedBackR=@"Above_Average";
    else if(meetingRating==1)
        feedBackR=@"Average";
    else if(meetingRating==2)
        feedBackR=@"Below_Average";
    else if(meetingRating==3)
        feedBackR=@"Did_not_happen";
    //int daysGroup=followGroup.getSelected;
    NSString *days=@"0";
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *dt = [dateFormatter dateFromString:noDays];
    NSString *tdyStr=[dateFormatter stringFromDate:[NSDate date]];
    NSDate *today=[dateFormatter dateFromString:tdyStr];
    if ([companyNme isEqualToString:@""]) {
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter company name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if([noDays isEqualToString:@""]){
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if([dt compare:today]==NSOrderedAscending){
        UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please select date from today" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        NSString *noOfDays=daysLabel.text;
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *url=checkOutUrl;       
        checkOutUrl=@"";
        NSString *urlStr=@"";
        urlStr=[urlStr stringByAppendingString:@"&user_id="];
        urlStr=[urlStr stringByAppendingString:appDelegate.userId];
        urlStr=[urlStr stringByAppendingString:@"&place="];
        urlStr=[urlStr stringByAppendingString:companyNme];
        urlStr=[urlStr stringByAppendingString:@"&rating="];
        urlStr=[urlStr stringByAppendingString:feedBackR];
        urlStr=[urlStr stringByAppendingString:@"&days_to_follow="];
        urlStr=[urlStr stringByAppendingString:days];
        urlStr=[urlStr stringByAppendingString:@"&follow_up_date="];
        urlStr=[urlStr stringByAppendingString:noOfDays];
        urlStr=[urlStr stringByAppendingString:@"&notes="];
        urlStr=[urlStr stringByAppendingString:feedbackComments]; 
        
        NSString *str=[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        url=[url stringByAppendingString:str];
        
        [feedbackView bringSubviewToFront:activityView];
        activityView.hidden=NO;
        [self performSelector:@selector(submitData:) withObject:url afterDelay:1];        
    }       
}

- (void) submitData:(NSString *)urlString{   
    urlString=[urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    NSURLResponse* response = nil;
    NSError *error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://automapicapp.com" withString:@"https://s2.automapicapp.com"];
        //NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://s2.automapicapp.com" withString:@"https://automapic.phpfogapp.com"];
        NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
        [request1 setHTTPMethod:@"GET"];
        NSURLResponse* response1 = nil;
        NSError *error1 = nil;
        NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
        NSString* responseText;
        responseText = [[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding];
        responseText=[responseText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([responseText isEqualToString:@"success"]){
            feedbackView.hidden=YES;
            afterFeedbackView.hidden=NO;
            activityView.hidden=YES;
        }else{
            activityView.hidden=YES;            
        }
    }else{
        NSString* responseText;
        responseText = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        responseText=[responseText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([responseText isEqualToString:@"success"]){
            /*
            EKEventStore* eventStore = [[EKEventStore alloc] init];
            if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
                // iOS 6 and later
                [eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
                     // If you don't perform your presentation logic on the
                     // main thread, the app hangs for 10 - 15 seconds.                    
                     if (granted) {
                         NSString *noOfDays=daysLabel.text;
                         NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                         [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                         NSDate *dt = [dateFormatter dateFromString:noOfDays];
                         EKReminder *reminder = [EKReminder reminderWithEventStore:eventStore];
                         reminder.title = @"Meeting with XYZ";                         
                         reminder.calendar = [eventStore defaultCalendarForNewReminders];
                         EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:dt];
                         [reminder addAlarm:alarm];
                         NSError *error1 = nil;
                         [eventStore saveReminder:reminder commit:YES error:&error1];
                         if (error1){
                             NSLog(@"error = %@", error1);
                             UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"error" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             [alert show];
                         }
                         else{
                             UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"Reminder added successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             [alert show];
                         }
                     }else{                         
                         UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Unable to add to calendar" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [alert show];
                     }          	           
                 }];
            } else {
                // iOS 5
                //NSLog(@"here");
                NSString *noOfDays=daysLabel.text;
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"dd/MM/yyyy"];
                NSDate *dt = [dateFormatter dateFromString:noOfDays];
                EKReminder *reminder = [EKReminder reminderWithEventStore:eventStore];
                reminder.title = @"Meeting with XYZ";
                reminder.calendar = [eventStore defaultCalendarForNewReminders];
                EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:dt];
                [reminder addAlarm:alarm];
                NSError *error1 = nil;
                [eventStore saveReminder:reminder commit:YES error:&error1];
                if (error1)
                    NSLog(@"error = %@", error);
            }
            */
            feedbackView.hidden=YES;
            afterFeedbackView.hidden=NO;
            activityView.hidden=YES;
        }else{
            activityView.hidden=YES;            
        }
    }    
}

- (IBAction)hideKeyboard:(id)sender {
    [sender resignFirstResponder];
}

- (void)dealloc {
    [feedbackView release];
    [companyName release];
    [feedbackNotes release];
    [daysSlider release];
    [daysLabel release];
    [checkoutHelp release];
    [afterFeedbackView release];
    [segmentControl release];
    [activityView release];
    [coordinatesLabel release];
    [captionLabel release];
    [refBtn release];
    [_refreshLabel release];
    [super dealloc];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }    
    return YES;
}


- (IBAction)onExitHideKeyBoard:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)daysChanged:(id)sender {
    //daysLabel.text=[NSString stringWithFormat:@"%i",(int)(daysSlider.value)];
}
@end
