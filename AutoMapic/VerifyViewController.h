//
//  VerifyViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 9/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJson.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>

@interface VerifyViewController : UITableViewController{
    UIView *activityView;
}
@property (retain, nonatomic) IBOutlet UITextField *phoneNumberText;
@property (retain, nonatomic) IBOutlet UITextField *groupNameText;
@property (retain, nonatomic) IBOutlet UITextField *companyNameText;
@property (retain, nonatomic) IBOutlet UITextField *passwordText;
@property (retain, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (retain, nonatomic) IBOutlet UIButton *checkedBoxButton;
@property (retain, nonatomic) IBOutlet UILabel *errorLabel;
@property (retain, nonatomic) IBOutlet UIButton *doneButton;
@property (retain, nonatomic) IBOutlet UIButton *backButton;


- (IBAction)forgotButtonClick:(id)sender;
- (IBAction)validate:(id)sender;
- (IBAction)onExitHideKeyboard:(id)sender;
- (void)keyboardDidHide:(NSNotification *)note;
- (IBAction)openLink:(id)sender;
- (void) submitData:(NSString *)urlString;
- (IBAction)backButtonClick:(id)sender;

@end
