//
//  CheckInViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 12/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "MIRadioButtonGroup.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+Base64.h"
#import <EventKit/EventKit.h>					

@interface CheckInViewController : UIViewController <CLLocationManagerDelegate,UIApplicationDelegate,UITextViewDelegate,UITextFieldDelegate>{
    IBOutlet UILabel *usernameLabel;
    IBOutlet UILabel *addressLabel;
    IBOutlet UIButton *checkInButton;
    IBOutlet UIButton *checkOutButton;
    CLLocationManager *locationManager;
    CLLocation *location;
    NSString *flag;
    MIRadioButtonGroup *group,*followGroup;
    NSString *checkOutUrl;
    UIActionSheet *dateSheet;
    UIView *activityView1;
    
}
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UIButton *checkInButton;
@property (strong, nonatomic) IBOutlet UIButton *checkOutButton;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *location;
@property (strong, nonatomic) IBOutlet UIButton *refreshIcon;
@property (nonatomic, retain) IBOutlet NSString *flag;
@property (nonatomic, retain) IBOutlet NSString *checkOutUrl;
@property (strong, nonatomic) IBOutlet UILabel *checkInActivityLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *checkInActivityIndicator;
@property (retain, nonatomic) IBOutlet UIScrollView *feedbackView;
@property (retain, nonatomic) IBOutlet UITextField *companyName;
@property (retain, nonatomic) IBOutlet UITextView *feedbackNotes;
@property (retain, nonatomic) IBOutlet UISlider *daysSlider;
@property (retain, nonatomic) IBOutlet UITextField *daysLabel;
@property (retain, nonatomic) IBOutlet UILabel *checkoutHelp;
@property (retain, nonatomic) IBOutlet UIView *afterFeedbackView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (retain, nonatomic) IBOutlet UIView *activityView;
@property (retain, nonatomic) IBOutlet UILabel *coordinatesLabel;
@property (retain, nonatomic) IBOutlet UILabel *captionLabel;
@property (retain, nonatomic) IBOutlet UIButton *refBtn;
@property (retain, nonatomic) IBOutlet UILabel *refreshLabel;

- (IBAction)refBtnClick:(id)sender;
- (IBAction)newMeeting:(id)sender;
- (IBAction)refresh:(id)sender;
- (IBAction)checkOutClicked:(id)sender;
- (IBAction)checkInClicked:(id)sender;
- (IBAction)onExitHideKeyBoard:(id)sender;
- (IBAction)daysChanged:(id)sender;
- (void)keyboardDidShow:(NSNotification *)note;
- (void)keyboardDidHide:(NSNotification *)note;
- (IBAction)feedBackSubmit:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
- (void) submitData:(NSString *)urlString;
- (void) setDate;
- (void) dismissDate;
- (void) cancelDate;
- (IBAction)settingsButtonClick:(id)sender;
- (IBAction)sendToSalesForce:(id)sender;
- (void) submitSalesForceData:(NSString *)urlString;
- (IBAction)valueChanged:(id)sender;
- (IBAction)sendToYammer:(id)sender;

@end
