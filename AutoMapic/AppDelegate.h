//
//  AppDelegate.h
//  AutoMapic
//
//  Created by Kiran Narayan on 11/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalyticsSession.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    NSString *userName;
    NSString *userId;
    NSString *insertId;
    NSString *companyId;
    NSString *groupId;
    NSString *phoneNumber;
    NSString *password;
    NSString *remember;
    NSString *sFlag;
    NSString *rFlag;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *insertId;
@property (strong, nonatomic) NSString *companyId;
@property (strong, nonatomic) NSString *groupId;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *remember;
@property (strong, nonatomic) NSString *sFlag;
@property (strong, nonatomic) NSString *rFlag;

@end
