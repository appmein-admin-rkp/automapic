//
//  RememberViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 10/8/12.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface RememberViewController : UIViewController
- (IBAction)clearButtonClick:(id)sender;
- (IBAction)backButtonClick:(id)sender;

@end
