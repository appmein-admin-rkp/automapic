//
//  ViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 11/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "SBJson.h"
#import "AppDelegate.h"
#import "Reachability.h"

@implementation ViewController{
    Reachability* internetReachable;
}
@synthesize tableView;
@synthesize phoneNumberLabel;
@synthesize phoneNumberText;
@synthesize passwordLabel;
@synthesize passwordText;
@synthesize checkboxImage;

@synthesize rememberMeLabel;
@synthesize okButton;
@synthesize checkBoxClickedImage;
@synthesize errorLabel;
@synthesize rememberUserLabel;
@synthesize yesButton;
@synthesize noButton;
@synthesize companyNameLabel;
@synthesize groupNameLabel;
@synthesize groupNameText;
@synthesize companyNameText;

-(void) yesButtonClicked:(id)sender{
    [self performSegueWithIdentifier:@"CheckInSeque" sender:self];
}
-(void) noButtonClicked:(id)sender{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
    NSMutableArray *users= [[NSMutableArray alloc] init];
    users= [NSMutableArray arrayWithContentsOfFile: plistPath];
    NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password", @"", @"companyname", @"", @"groupname",@"0",@"remember", nil];
    [users removeAllObjects];
    [users addObject: user];
    [users writeToFile:plistPath atomically:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
   
    /*
    phoneNumberLabel.hidden=NO;
    phoneNumberText.hidden=NO;
    passwordLabel.hidden=NO;
    passwordText.hidden=NO;
    companyNameLabel.hidden=NO;
    companyNameText.hidden=NO;
    groupNameLabel.hidden=NO;
    groupNameText.hidden=NO;
    rememberMeLabel.hidden=NO;
    okButton.hidden=NO;
    errorLabel.hidden=YES;
    checkboxImage.hidden=NO;
    checkBoxClickedImage.hidden=YES;
    rememberUserLabel.hidden=YES;
    yesButton.hidden=YES;
    noButton.hidden=YES;
     */
}

-(void) checkBoxClicked:(id)sender{
    checkboxImage.hidden=NO;
    checkBoxClickedImage.hidden=YES;
    
}

-(void)checkBoxClick:(id)sender{
    checkBoxClickedImage.hidden=NO;
    checkboxImage.hidden=YES;
}

- (IBAction)onExitHideKeyboard:(id)sender {
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    [sender resignFirstResponder];
}

-(void)validate:(id)sender{    
    [sender resignFirstResponder];
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    NSString *phoneNumber=[phoneNumberText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password=[passwordText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *company=[companyNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *group=[groupNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    errorLabel.text=@"";
    errorLabel.hidden=TRUE;    
    if([phoneNumber length]<=0){
        errorLabel.text=@"Please enter phone number";
        errorLabel.hidden=FALSE;        
    }    
    else if([password length]<=0){
        errorLabel.text=@"Please enter password";
        errorLabel.hidden=FALSE;        
    }
    else if([company length]<=0){
        errorLabel.text=@"Please enter company name";
        errorLabel.hidden=FALSE;        
    }
    else if([group length]<=0){
        errorLabel.text=@"Please enter group name";
        errorLabel.hidden=FALSE;        
    }
    else{
      //  NSString *url=[@"http://www.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
      //  NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
         NSString *url=[@"https://automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
        url=[url stringByAppendingString:@"&password="];
        url=[url stringByAppendingString:password];
        url=[url stringByAppendingString:@"&company_name="];
        url=[url stringByAppendingString:company];
        url=[url stringByAppendingString:@"&group_name="];
        url=[url stringByAppendingString:group];
        url=[url stringByAppendingString:@"&param=mobile_phonecheck"];
        
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        //   NSString *params = [[NSString alloc] initWithFormat:@"cname=bar&uname=value&password=pass&param=admincheck"];
        [request setHTTPMethod:@"GET"];
        // [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLResponse* response = nil;
        NSError *error = nil;
        NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString* responseText;
        responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
       // responseText=[response 
        //NSLog(responseText);
        if ([responseText isEqualToString:@"not match"])
        {
            errorLabel.text=@"Please check the credentials";
            errorLabel.hidden=FALSE; 
        }
        else{            
            NSDictionary *results= [responseText JSONValue];
            NSMutableArray *result= [results objectForKey:@"result"];
            NSDictionary *user=[result objectAtIndex:0];
            AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.userName=[user objectForKey:@"username"];
            appDelegate.userId=[user objectForKey:@"id"];
            appDelegate.companyId=[user objectForKey:@"company_id"];
            appDelegate.groupId=[user objectForKey:@"group_id"];
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];           
            
            NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
            // NSLog(plistPath);
            
            NSMutableArray *users= [[NSMutableArray alloc] init];
            users= [NSMutableArray arrayWithContentsOfFile: plistPath];
            if(checkBoxClickedImage.hidden){
                NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber",@"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                [users removeAllObjects];
                [users addObject: user];
            }
            else{
                 NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: phoneNumber, @"phonenumber", password, @"password",company, @"companyname",group, @"groupname",@"1",@"remember", nil];
                [users removeAllObjects];
                [users addObject: user];
                //   UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Info" message:@"checked" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                //  [alert show];
            }
            [users writeToFile:plistPath atomically:YES];            
            [self performSegueWithIdentifier:@"CheckInSeque" sender:self];
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    /* keyboard is visible, move views */
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
         self.view.frame=CGRectMake(0, -180, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
         self.view.frame=CGRectMake(0, 180, self.view.frame.size.width, self.view.frame.size.height);
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    /* resign first responder, hide keyboard, move views */
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
    //self.view.center=CGPointMake(0, -200);
}

#pragma mark - View lifecycle

- (void)keyboardDidShow:(NSNotification *)note 
{
    /* move your views here */
   // self.view.center=CGPointMake(0, 0);
}
- (void)keyboardDidHide:(NSNotification *)note 
{
    /* move your views here */
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
}

- (void)viewDidLoad
{   
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *userName=@"Welcome back, ";
    userName=[userName stringByAppendingString:appDelegate.userName];
    rememberUserLabel.text=userName;
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_image.png"]];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    passwordText.delegate=self;
    //self.view.center=CGPointMake(0, 0);
    tableView.delegate=self;
    tableView.dataSource=self;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"user.plist"];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        NSMutableArray *users= [[NSMutableArray alloc] init];
        users= [NSMutableArray arrayWithContentsOfFile: plistPath];
        NSDictionary *user=[users objectAtIndex:0];
        NSString *remember= [user objectForKey:@"remember"];
        if([remember isEqualToString:@"1"]){
            
            NSString *phoneNumber= [user objectForKey:@"phonenumber"];
            NSString *password= [user objectForKey:@"password"];
            NSString *company= [user objectForKey:@"companyname"];
            NSString *group= [user objectForKey:@"groupname"];
            
            //NSString *url=[@"http://www.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
            //NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
            NSString *url=[@"https://automapic.phpfogapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
            url=[url stringByAppendingString:@"&password="];
            url=[url stringByAppendingString:password];
            url=[url stringByAppendingString:@"&company_name="];
            url=[url stringByAppendingString:company];
            url=[url stringByAppendingString:@"&group_name="];
            url=[url stringByAppendingString:group];
            url=[url stringByAppendingString:@"&param=mobile_phonecheck"];            
            
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            //   NSString *params = [[NSString alloc] initWithFormat:@"cname=bar&uname=value&password=pass&param=admincheck"];
            [request setHTTPMethod:@"GET"];
            // [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLResponse* response = nil;
            NSError *error = nil;
            NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSString* responseText;
            responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            // responseText=[response 
            if ([responseText isEqualToString:@"not match"])
            {                
                NSDictionary *userNew = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                [users removeAllObjects];
                [users addObject: userNew];
                [users writeToFile:plistPath atomically:YES];
            }
            else{
                NSDictionary *results= [responseText JSONValue];
                NSMutableArray *result= [results objectForKey:@"result"];
                NSDictionary *user=[result objectAtIndex:0];
                AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelegate.userName=[user objectForKey:@"username"];
                appDelegate.userId=[user objectForKey:@"id"];
                appDelegate.companyId=[user objectForKey:@"company_id"];
                appDelegate.groupId=[user objectForKey:@"group_id"];
                
                NSString *userName=@"You are ";
                userName=[userName stringByAppendingString:appDelegate.userName];                
                self.rememberUserLabel.text=userName;                
                phoneNumberLabel.hidden=YES;
                phoneNumberText.hidden=YES;
                passwordLabel.hidden=YES;
                passwordText.hidden=YES;
                companyNameLabel.hidden=YES;
                companyNameText.hidden=YES;
                groupNameLabel.hidden=YES;
                groupNameText.hidden=YES;
                rememberMeLabel.hidden=YES;
                okButton.hidden=YES;
                errorLabel.hidden=YES;
                checkboxImage.hidden=YES;
                checkBoxClickedImage.hidden=YES;
                rememberUserLabel.hidden=NO;
                yesButton.hidden=NO;
                noButton.hidden=NO;
            }            
        }    
        else{
            
        }
	} else {       
        NSMutableArray *users= [[NSMutableArray alloc] init];
        NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
        [users addObject: user];
        [users writeToFile:plistPath atomically:YES];
    }
     */
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void) checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)    
    {
        case NotReachable:
        {
            UIAlertView *alert= [[UIAlertView alloc] initWithTitle:@"Error" message:@"Internet connection required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];  
            break;            
        }
        case ReachableViaWiFi:
        {
            break;            
        }
        case ReachableViaWWAN:
        {
           break;            
        }
    }
}

-(void) viewWillAppear:(BOOL)animated{
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
     */
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];    
}

- (void)viewDidUnload
{
    [self setPhoneNumberLabel:nil];
    [self setPhoneNumberText:nil];
    [self setPasswordLabel:nil];
    [self setPasswordText:nil];
    [self setRememberMeLabel:nil];
    [self setOkButton:nil];
    [self setErrorLabel:nil];
    [self setCheckboxImage:nil];
    [self setCheckBoxClickedImage:nil];
    [self setCompanyNameLabel:nil];
    [self setGroupNameLabel:nil];
    [self setGroupNameText:nil];
    [self setCompanyNameText:nil];
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
        return YES;
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    else
        return NO;
}  

- (void)dealloc {
    [companyNameLabel release];
    [groupNameLabel release];
    [groupNameText release];
    [companyNameText release];
    [tableView release];
    [super dealloc];
}
@end
