//
//  SalesForceViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 10/8/12.
//
//

#import "SalesForceViewController.h"

@interface SalesForceViewController ()

@end

@implementation SalesForceViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.tableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_image.png"]];
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
        return YES;
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    else
        return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
 */

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

- (void)dealloc {
    [_username release];
    [_password release];
    [_secretToken release];
    [_integrateBtnClick release];
    [_errorLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setUsername:nil];
    [self setPassword:nil];
    [self setSecretToken:nil];
    [self setIntegrateBtnClick:nil];
    [self setErrorLabel:nil];
    [super viewDidUnload];
}
- (IBAction)backButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)integrateButtonClick:(id)sender {
    [sender resignFirstResponder];
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.view.frame=CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
        
    }else if (self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    NSString *usernameT=[self.username.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordT=[self.password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *tokenT=[self.secretToken.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    				
    self.errorLabel.text=@"";
    self.errorLabel.hidden=TRUE;
    
    usernameT=[usernameT stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    passwordT=[passwordT stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    tokenT=[tokenT stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    if([usernameT length]<=0){
        self.errorLabel.text=@"Please enter username";
        self.errorLabel.hidden=FALSE;
    }
    else if([passwordT length]<=0){
        self.errorLabel.text=@"Please enter password";
        self.errorLabel.hidden=FALSE;
    }
    else if([tokenT length]<=0){
        self.errorLabel.text=@"Please enter secret token";
        self.errorLabel.hidden=FALSE;
    }else{
        NSString *url=[@"https://automapicapp.com/mobile.php?user_name=" stringByAppendingString:usernameT];
        url=[url stringByAppendingString:@"&password="];
        url=[url stringByAppendingString:passwordT];
        url=[url stringByAppendingString:@"&security_token="];
        url=[url stringByAppendingString:tokenT];
        url=[url stringByAppendingString:@"&param=salesforce_auth"];
        int x=self.view.frame.size.width/2-50;
        int y=self.view.frame.size.height/2-44;
        activityView = [[UIView alloc] initWithFrame:CGRectMake(x, y, 100, 88)];
        activityView.backgroundColor=[UIColor lightGrayColor];
        activityView.alpha=0.6;
        [activityView.layer setCornerRadius:5.0f];
        UIActivityIndicatorView *activityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.alpha = 1.0;
        activityIndicator.color=[UIColor colorWithRed:76/255.0 green:76/255.0 blue:76/255.0 alpha:1.0];
        activityIndicator.frame=CGRectMake(32, 13, 37, 37);
        [activityIndicator startAnimating];
        [activityView addSubview:activityIndicator];
        
        UILabel *message=[[UILabel alloc] initWithFrame:CGRectMake(0, 58, 100, 21)] ;
        message.text=@"Loading...";
        //userName.textColor
        message.opaque=NO;
        message.backgroundColor=[UIColor clearColor];
        message.textColor=[UIColor colorWithRed:76/255.0 green:76/255.0 blue:76/255.0 alpha:1.0];
        message.textAlignment=UITextAlignmentCenter;
        message.font=[UIFont fontWithName:@"Verdana-Bold" size:15];
        message.adjustsFontSizeToFitWidth=NO;
        [activityView addSubview:message];
        [self.view addSubview:activityView];
        [self performSelector:@selector(submitData:) withObject:url afterDelay:1];
    }
}

- (void) submitData:(NSString *)urlString{
    NSString *usernameT=[self.username.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordT=[self.password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *tokenT=[self.secretToken.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    usernameT=[usernameT stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    passwordT=[passwordT stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    NSString *passStr=[passwordT base64EncodedString];
    tokenT=[tokenT stringByReplacingOccurrencesOfString:@" " withString:@"__"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"GET"];
    NSURLResponse* response = nil;
    NSError *error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://automapicapp.com" withString:@"https://s2.automapicapp.com"];
        //NSString *urlString1 = [urlString stringByReplacingOccurrencesOfString:@"https://s2.automapicapp.com" withString:@"https://automapic.phpfogapp.com"];
        NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString1]];
        [request1 setHTTPMethod:@"GET"];
        NSURLResponse* response1 = nil;
        NSError *error1 = nil;
        NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
        NSString *responseText1;
        responseText1 = [[[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([responseText1 isEqualToString:@"no"])
        {
            self.errorLabel.text=@"Please check the credentials";
            self.errorLabel.hidden=FALSE;
            activityView.hidden=YES;
            activityView=nil;
        }
        else if([responseText1 isEqualToString:@"yes"]){
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"salesforce.plist"];
            NSMutableArray *users= [[NSMutableArray alloc] init];
            users= [NSMutableArray arrayWithContentsOfFile: plistPath];
            NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys:usernameT, @"username",passStr, @"password",tokenT, @"token",@"1",@"status", nil];
            [users removeAllObjects];
            [users addObject: user];
            [users writeToFile:plistPath atomically:YES];
            activityView.hidden=YES;
            activityView=nil;
            /*
             UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
             UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceRemember"];
             [self presentViewController:myController animated:YES completion:nil];
             //[self performSegueWithIdentifier:@"MainViewSeque" sender:self];
             */
            AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.sFlag=@"1";
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            activityView.hidden=YES;
            activityView=nil;
            self.errorLabel.text=@"Please check the credentials";
            self.errorLabel.hidden=FALSE;
        }
    }
    else{
        NSString* responseText;
        responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([responseText isEqualToString:@"no"])
        {
            self.errorLabel.text=@"Please check the credentials";
            self.errorLabel.hidden=FALSE;
            activityView.hidden=YES;
            activityView=nil;
        }
        else if([responseText isEqualToString:@"yes"]){
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"salesforce.plist"];
            NSMutableArray *users= [[NSMutableArray alloc] init];
            users= [NSMutableArray arrayWithContentsOfFile: plistPath];
            NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys:usernameT, @"username",passStr, @"password",tokenT, @"token",@"1",@"status", nil];
            [users removeAllObjects];
            [users addObject: user];
            [users writeToFile:plistPath atomically:YES];
            activityView.hidden=YES;
            activityView=nil;
            /*
            UIStoryboard *storyBoard= [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController *myController = [storyBoard instantiateViewControllerWithIdentifier:@"SalesForceRemember"];
            [self presentViewController:myController animated:YES completion:nil];
            //[self performSegueWithIdentifier:@"MainViewSeque" sender:self];
             */
            AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.sFlag=@"1";
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            activityView.hidden=YES;
            activityView=nil;
            self.errorLabel.text=@"Please check the credentials";
            self.errorLabel.hidden=FALSE;
        }
    }
}
- (IBAction)hideKeyboard:(id)sender {
    [sender resignFirstResponder];
}
@end
