//
//  YammerViewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 12/12/12.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface YammerViewController : UIViewController<UIWebViewDelegate>
@property (retain, nonatomic) IBOutlet UIView *sampl;
@property (retain, nonatomic) IBOutlet UIWebView *yammerView;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)backButtonClick:(id)sender;

@end
