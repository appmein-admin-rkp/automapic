//
//  RememberViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 10/8/12.
//
//

#import "RememberViewController.h"

@interface RememberViewController ()

@end

@implementation RememberViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_image.png"]];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if(interfaceOrientation == UIInterfaceOrientationPortrait)
        return YES;
    else if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        return YES;
    else
        return NO;
}

- (IBAction)clearButtonClick:(id)sender {
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"salesforce.plist"];
    NSMutableArray *users= [[NSMutableArray alloc] init];
    NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"username",@"", @"password",@"", @"token",@"0",@"status", nil];
    [users removeAllObjects];
    [users addObject: user];
    [users writeToFile:plistPath atomically:YES];
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.rFlag=@"1";
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
