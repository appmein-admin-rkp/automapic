//
//  Table viewController.h
//  AutoMapic
//
//  Created by Kiran Narayan on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJson.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>

@class Reachability;

@interface loginViewController : UITableViewController<UITextFieldDelegate>{
    UIView *activityView;
    int flag;
}
@property (retain, nonatomic) IBOutlet UITextField *phoneNumberText;
@property (retain, nonatomic) IBOutlet UITextField *groupNameText;
@property (retain, nonatomic) IBOutlet UITextField *companyNameText;
@property (retain, nonatomic) IBOutlet UITextField *passwordText;
@property (retain, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (retain, nonatomic) IBOutlet UIButton *checkedBoxButton;
@property (retain, nonatomic) IBOutlet UILabel *errorLabel;
@property (retain, nonatomic) IBOutlet UIButton *doneButton;
@property (retain, nonatomic) IBOutlet UITableView *mainView;


- (IBAction)forgotButtonClick:(id)sender;
- (IBAction)validate:(id)sender;
- (IBAction)checkBoxClick:(id)sender;
- (IBAction)checkedBoxClick:(id)sender;
- (IBAction)onExitHideKeyboard:(id)sender;
- (void)keyboardDidHide:(NSNotification *)note;
- (IBAction)openLink:(id)sender;
- (void) checkNetworkStatus:(NSNotification *)notice;
- (void) submitData:(NSString *)urlString;
- (void) addButtonToKeyboard;
@end
