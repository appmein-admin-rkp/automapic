//
//  YammerViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 12/12/12.
//
//

#import "YammerViewController.h"

@interface YammerViewController ()

@end

@implementation YammerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.yammerView.delegate=self;
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlG=@"https://automapicapp.com/yammer_integration/yammer_redirect.php";
    urlG=[urlG stringByAppendingFormat:@"?last_inserted_id="];
    urlG=[urlG stringByAppendingFormat:appDelegate.insertId];
    urlG=[urlG stringByAppendingFormat:@"&group_id="];
    urlG=[urlG stringByAppendingFormat:appDelegate.groupId];
    urlG=[urlG stringByAppendingFormat:@"&user_id="];
    urlG=[urlG stringByAppendingFormat:appDelegate.userId];
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:[NSURL URLWithString:urlG]];
    [self.yammerView loadRequest:requestURL];
	// Do any additional setup after loading the view.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_sampl release];
    [_yammerView release];
    [_activityIndicator release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setSampl:nil];
    [self setYammerView:nil];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
}
- (IBAction)backButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
