//
//  MainViewController.m
//  AutoMapic
//
//  Created by Kiran Narayan on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_image.png"]];
    [super viewDidLoad];
}


- (void)viewDidAppear:(BOOL)animated
{
     NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
     //NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"user.plist"];
     NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:@"details.plist"];
     if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
         NSMutableArray *users= [[NSMutableArray alloc] init];
         users= [NSMutableArray arrayWithContentsOfFile: plistPath];
         NSDictionary *user=[users objectAtIndex:0];
         NSString *remember= [user objectForKey:@"remember"];
         if([remember isEqualToString:@"1"]){             
                NSString *phoneNumber= [user objectForKey:@"phonenumber"];
                NSString *password= [user objectForKey:@"password"];
                NSString *company= [user objectForKey:@"companyname"];
                NSString *group= [user objectForKey:@"groupname"];
     
                //NSString *url=[@"http://www.automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
                //NSString *url=[@"http://ec2-107-22-84-11.compute-1.amazonaws.com/automapicapp.com/mobile.php?phone=" stringByAppendingString:phoneNumber];
                
                NSString *urlStr=[@"?phone=" stringByAppendingString:phoneNumber];
                urlStr=[urlStr stringByAppendingString:@"&password="];
                urlStr=[urlStr stringByAppendingString:password];
                urlStr=[urlStr stringByAppendingString:@"&company_name="];
                urlStr=[urlStr stringByAppendingString:company];
                urlStr=[urlStr stringByAppendingString:@"&group_name="];
                urlStr=[urlStr stringByAppendingString:group];
                urlStr=[urlStr stringByAppendingString:@"&param=mobile_phonecheck"];            
             
             NSString *url=[@"https://automapicapp.com/mobile.php" stringByAppendingString:urlStr];
                //NSString *url=[@"https://s2.automapicapp.com/mobile.php" stringByAppendingString:urlStr];
                NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
                [request setHTTPMethod:@"GET"];
                NSURLResponse* response = nil;
                NSError *error = nil;
                NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
             
                if (error) {
                    NSString *url1=[@"https://s2.automapicapp.com/mobile.php" stringByAppendingString:urlStr];
                    //NSString *url1=[@"https://automapic.phpfogapp.com/mobile.php" stringByAppendingString:urlStr];
                    NSMutableURLRequest *request1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
                    [request1 setHTTPMethod:@"GET"];
                    NSURLResponse* response1 = nil;
                    NSError *error1 = nil;
                    NSData* data1 = [NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
                    NSString* responseText;
                    responseText = [[[NSString alloc] initWithData:data1 encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    // responseText=[response 
                    if ([responseText isEqualToString:@"not match"])
                    {                  
                        NSDictionary *userNew = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                        [users removeAllObjects];
                        [users addObject: userNew];
                        [users writeToFile:plistPath atomically:YES];  
                        [self performSegueWithIdentifier:@"MainLoginSeque" sender:self]; 
                    }
                    else if([responseText rangeOfString:@"result"].location != NSNotFound){
                        NSDictionary *results= [responseText JSONValue];
                        NSMutableArray *result= [results objectForKey:@"result"];
                        NSDictionary *user=[result objectAtIndex:0];
                        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appDelegate.userName=[user objectForKey:@"username"];
                        appDelegate.userId=[user objectForKey:@"id"];
                        appDelegate.companyId=[user objectForKey:@"company_id"];
                        appDelegate.groupId=[user objectForKey:@"group_id"];                
                        [self performSegueWithIdentifier:@"MainRememberSeque" sender:self];                
                    }else{
                        NSDictionary *userNew = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                        [users removeAllObjects];
                        [users addObject: userNew];
                        [users writeToFile:plistPath atomically:YES];  
                        [self performSegueWithIdentifier:@"MainLoginSeque" sender:self];                
                    }
                    
                } else{
                    NSString* responseText;
                    responseText = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    // responseText=[response 
                    if ([responseText isEqualToString:@"not match"])
                    {                  
                        NSDictionary *userNew = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                        [users removeAllObjects];
                        [users addObject: userNew];
                        [users writeToFile:plistPath atomically:YES];  
                        [self performSegueWithIdentifier:@"MainLoginSeque" sender:self]; 
                    }
                    else if([responseText rangeOfString:@"result"].location != NSNotFound){
                        NSDictionary *results= [responseText JSONValue];
                        NSMutableArray *result= [results objectForKey:@"result"];
                        NSDictionary *user=[result objectAtIndex:0];
                        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        appDelegate.userName=[user objectForKey:@"username"];
                        appDelegate.userId=[user objectForKey:@"id"];
                        appDelegate.companyId=[user objectForKey:@"company_id"];
                        appDelegate.groupId=[user objectForKey:@"group_id"];                
                        [self performSegueWithIdentifier:@"MainRememberSeque" sender:self];                
                    }else{
                        NSDictionary *userNew = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
                        [users removeAllObjects];
                        [users addObject: userNew];
                        [users writeToFile:plistPath atomically:YES];  
                        [self performSegueWithIdentifier:@"MainLoginSeque" sender:self];                
                    }
                }                           
         }    
         else{
             [self performSegueWithIdentifier:@"MainLoginSeque" sender:self];
         }
     }else {       
         NSMutableArray *users= [[NSMutableArray alloc] init];
         NSDictionary *user = [[NSDictionary alloc] initWithObjectsAndKeys: @"", @"phonenumber", @"", @"password",@"", @"companyname",@"", @"groupname",@"0",@"remember", nil];
         [users addObject: user];
         [users writeToFile:plistPath atomically:YES];
         [self performSegueWithIdentifier:@"MainLoginSeque" sender:self]; 
    }     
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
